# Base image
FROM ubuntu:18.04

# Maintainer
MAINTAINER Quentin Buiteman <hello@quentinbuiteman.com>

# Update and upgrade
RUN apt update \
    && apt upgrade -y

# OpenSSH
RUN apt install openssh-client -y

# Yarn
RUN apt install nodejs -y \
    && apt install npm -y \
    && apt remove cmdtest \
    && apt remove yarn \
    && npm install -g yarn
