# Docker Deploy
Small Dockerfile based on Ubuntu 18.04, used for deployment with Bitbucket Pipelines. Contains the following tools:

* SSH
* Yarn

## Usage
Use this Docker image to build your JavaScript with Bitbucket Pipelines. Use SSH to deploy to your server.
